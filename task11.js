function variables(number1,number2)
{
    var leapyear=[];
    var i;
    for(i=number1;i<=number2;i++)
    {
        var leap=0;
        if(i%4===0 && i%100!==0)
        {
            leap=1;
        }
        else if(i%100===0 && i%400===0)
        {
            leap=1;
        }
        else{
            leap=0;
        }
        if(leap===1)
        {
            leapyear.push(i);
        }
    }
    return(leapyear);
}
module.exports=variables;
