function binarysearch(arr,n,first,last)
{
    if(last>=first)
    {
    var mid=parseInt((first+last)/2);
    if(n===arr[mid])
    {
        return(true);
    }
    else if(n>arr[mid])
    {
        return(binarysearch(arr,n,mid+1,last));
    }
    else if(n<arr[mid])
    {
        return(binarysearch(arr,n,first,mid-1));
    }
    }
    else{
        return(false);
    }
}
module.exports=binarysearch;
